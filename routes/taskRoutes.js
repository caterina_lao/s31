//Contain all task endpoints for our applications

const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskControllers')
const newTask = require('../controllers/taskControllers')

router.get('/', (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

})

router.post('/createTask' ,(req, res) => {

	taskController.createTask(req.body).then(resultFromController =>
		res.send(resultFromController))
	
})

//route for deleting task

router.delete('/deleteTask/:id', (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//route for updating task

router.put('/updateTask/:id', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// Activity

//1 4
router.get('/:id', (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//5-8
router.put('/:id/complete', (req, res) => {
	taskController.updatedStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router