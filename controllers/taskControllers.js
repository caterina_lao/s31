//contain all business logic

const Task = require('../models/taskSchema');

//Get All Tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

//Creating New Task

module.exports.createTask = (reqBody) => {

		let newTask = new Task({
			name:reqBody.name
		})

		return newTask.save().then((task,err) => {
			if(err) {
				console.log(err)
				return false
			} else {
				return task
			}
		})
}

// Delete task 

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((removedTask, err) => {

		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

//update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updateTask
				}
			})
	})
}

// Activity

//1 -4


module.exports.getSpecificTask = (taskId) => {

	return Task.findById(taskId).then((err, result) => {

		if (err) {

			return console.log(err);
			return false
		} else {

			return getSpecificTask
		}
	})
}

//5-8
module.exports.updatedStatus = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.status = newContent.status
			return result.save().then((updatedStatus, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedStatus
				}
			})
	})
}









	
